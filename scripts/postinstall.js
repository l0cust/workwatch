#!/usr/bin/env node
/**
 * This file is part of the WorkWatch, a Linux terminal program for honest
 * worktime tracking and billing.
 *
 * Copyright (C) 2020-2025 by Artur Rutkowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * WorkWatch is beeing developped and maintained by Artur (locust) Rutkwoski
 * <locust@mailbox.org>
 */

/**
 * This file is not part of the WorkWatch's sorce code. It is run when
 * the WorkWatch has been installed in order to create the data directory.
 */
const fs = require("node:fs");
const process = require("node:process");
const {Config} = require("../src/config.js");

function checkForDataDir() {
  return new Promise((resolve, reject) => {
    let config;
    // Check if there is any config file in predefined locations including
    // WORKWATCH_CONFIG_FILE environment variable.
    Config.load()
    .catch(configLoadError => {
      if (configLoadError.code === "ERR_NO_CONFIG") {
        // Instantiate config object consisting predefined values including
        // environment variables.
        return new Config();
      } else {
        reject(configLoadError);
      }
    })
    .then(configObject => {
      config = configObject;
      // Validate config data. No matter if it is loaded from file or based
      // only on predefined values.
      return config.verify();
    })
    .catch(configVerifyError => {
      if (configVerifyError.code === "ERR_CONFIG_DATA_DIR_NOT_EXISTS") {
        fs.mkdir(config.dataDir, {recursive: true}, (error) => {
          if (error) {
            reject(error);
          }
          resolve(config.dataDir);
        });
      } else {
        reject(configVerifyError);
      }
    })
    .then(configVerifySuccess => {
      if (typeof configVerifySuccess === "boolean" && configVerifySuccess) {
        resolve(config.dataDir);
      }
    });
  });
}

checkForDataDir()
.then(path => {})
.catch(error => {
  if (error instanceof Error) {
    process.exitCode = 1;
  }
});
