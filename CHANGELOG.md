# Changelog

## [Version 0.0.1](https://gitlab.com/l0cust/workwatch/-/milestones/1) (2024-06-18)

* Fixed file-handling objects' hierarchy ([#1](https://gitlab.com/l0cust/workwatch/-/issues/1))
* Added basic file-loading optimisation ([#2](https://gitlab.com/l0cust/workwatch/-/issues/2))
* Improved invoking detection ([#3](https://gitlab.com/l0cust/workwatch/-/issues/3))
* Edited user guide to be more friendly
