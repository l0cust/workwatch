# WorkWatch

WorkWatch is a Linux terminal program for honest worktime tracking and billing. It is a tool for help, not for enforcing anything. It is free software and is distributed under the terms of GNU General Public License.

The program's name is a combination of "work" and "stopwatch" words. It really does like a stopwatch but measuring hours and minutes.

## Get Started

It is pure JavaScript and Node.js application so you need npm to install. Install it globally for convenient use.

```sh
$ npm install -g workwatch
```

It will install the program and create its data directory used for data files. If you are making an update the data directory shouldn't be harmed during procedure.

To start your first measurement just type:

```sh
$ workwatch start
```

After you finished your work just go to the program's screen and press <kbd>CTRL+S</kbd> to stop measurement. If you want it to be stored in your worklog type the following command:

```sh
$ workwatch reset
```

Agree to store it and assing it a project or task you were working on. If you want to view the entire worklog type:

```sh
$ workwatch log
```

That's all.

## Documentation

For full program usage, see the [Users Guide](https://gitlab.com/l0cust/workwatch/-/blob/master/docs/index.md).

For further program development see the project's issues.
