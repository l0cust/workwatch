# 5. Files

The WorkWatch works with two files:

1. measurement - a JSON file containing current measurement,
2. log - a JSON file containing entire worklog.

Both files are in JSON format. They are stored inside the data directory (usually `$HOME/.workwatch`). The measurement file is used during measuring your time. It is required to store measurement's data between breaks made by stopping and resuming measurement. It is removed when the measurement is reset.

The measurement file contains following fields:

- time - a measured time in `hh:mm` format,
- number - an integer, greater than 0 number of measurement for the particular day,
- hoursRange - the array of `{"from": "hh:mm", "to": "hh:mm"}` object marking hours of your work,
- project - a string naming a project you want to assing this measurement.

The project field is fully aggremental for now. You can type anything but it'll be better to type a project name or a task you are working on. In later versions it will help you to find the worklog entries concerning this project or task. This field will be also used to integrate program with time-tracking and projest-management systems.

The log file contains all your worktime measured previously. It is an array of objects containing the following fields:

- day - a string containing date of measures in `yyyy-mm-dd` format,
- measurements - an array of measures done this particular day.

The measurements array contains the objects of measurements listed above. Each day may have some amount of measurements but they can't excess 23 hours and 59 minutes. The program will refuse to load such log if it contains more time measured in particular day than possible.
