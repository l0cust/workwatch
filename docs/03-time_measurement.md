# 3. Time Measurement

Current version of WorkWatch (0.0.1) has three basic commands to manage the measurement:

1. "start" - begins a new measurement or continues the one began previously,
2. "reset" - resets measurement and optionally stores it in the log,
3. "log" - views the entire worklog.

To start a new measurement type the following command:

```sh
$ workwatch start
```

It starts a WorkWatch in an interactive mode. During the measurement, the current time is displayed on the status line (the last line of your screen). Beside that, application informs you which number of measurement has been started or beeing continued and lists all hours you've worked up to the current measurement. The measurement is continuous. It means that your full worked time will be displayed when you resume stopped measurement.

You can make an unlimited amount of measurements unless they excess 23 hours and 59 minutes. Therefore the number of measurement is given and stored in the measurement file and later stored in the log.

To stop the measurement press <kbd>^C</kbd> (<kbd>CTRL+C</kbd>). Invoke the "start" command again to resume.

You can't make measurement which excess a previously mentioned time limit. If the real time reaches the 23:59, the program will stop automatically. Additionally, you can't continue your measurement next day or just after some days after you finished one.

To enable further work type:

```sh
$ workwatch reset
```

This command resets your measurement and optionally saves it in your worklog.

> Warning: command won't ask you if you agree to reset your measurement. It asks for saving measurement in the log. You can cancel the dialog screen with <kbd>^C</kbd>.

If you confirm, the command will ask you to name the project to which your measurement will be assigned. If you press enter without giving a name the "None" word will be used as a project name. Again, you can cancel here by pressing <kbd>^C</kbd>.

If you disagree, the measurement will be reset withut saving it in your worklog. To view your worklog just type:

```sh
$ workwatch log
```

The log command is non-interactive. It just displays your worklog as a table. You can use this command with pagers freely. Commands mentioned previously are interactive and they require a terminal.

The table of the worklog contains five columns:

- Day - the day of measurement you've made.
- Worked - the time you've worked.
- Number - a measurement sequential number,
- Hours - the range of hours at which you were working,
- Project - a project you are working on.

If you made a few measurements on one day, they'll be displayed sequentially from oldest to newest. You can recognise it by seeing the same Day column and that the Number column is incrementing.

The Hours column is the range of hours in which you were working. If you started your measurement at 10:00, and stopped it at 15:30, these hours will appear if you store this measurement in the log. If you made some breaks by stopping and resuming the measurement, the entire column will takes more than one screen line because it displays one range under another. So, the single worklog entry may take several screen lines.
