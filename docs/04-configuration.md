# 4. Configuration

WorkWatch doesn't have much configuration. Currently it contains only four parameters. You can provide them using command-line interface, a config file and the environment variables.

The above order is used to determine where the config data are taken from. The command-line and environment variables can accept the config file location. Generally, the config file is searched in the data directory or in a `.config/workwatch/` directory in your home path. If you have your own config file you can type:

```sh
$ workwatch --config-file your/config/file.json ...
```

or:

```sh
export WORKWATCH_CONFIG_FILE=your/config/file.json
$ workwatch ...
```

If there are no parameters provided in any way the program uses predefined values. All parameters, their supplying variants and predefined values are described in the table below.

```json:table
{
  "fields": [
    {"key": "param", "label": "Parameter Name"},
    {"key": "cli", "label": "Command-Line Option"},
    {"key": "config", "label": "Config File Option"},
    {"key": "env", "label": "Environment Variable"},
    {"key": "desc", "label": "Description"}
  ],
  "items": [
    {
      "param": "config file",
      "cli": "--config-file",
      "config": "N/A",
      "env": "WORKWATCH_CONFIG_FILE",
      "desc": "The path to the config file defining all parameters left in the rows below."
    },
    {
      "param": "measurement filename",
      "cli": "--measurement-filename",
      "config": "measurementFilename",
      "env": "WORKWATCH_MEASUREMENT_FILENAME",
      "desc": "The measurement file name without extension. It accepts only name, not the absolute or relative file path. The given name is used to store measurement file under this name with the .json extension. If not present the 'measurement' value is used."
    },
    {
      "param": "log filename",
      "cli": "--log-filename",
      "config": "logFilename",
      "env": "WORKWATCH_LOG_FILENAME",
      "desc": "The log file name without extension. It accepts only a name, not the absolute or relative file path. It is used to store the worklog file under this name with .json extension. If not present the 'log' value is used."
    },
    {
      "param": "data dir",
      "cli": "--data-dir",
      "config": "dataDir",
      "env": "WORKWATCH_DATA_DIR",
      "desc": "An absolute path to data directory. Program stores all its data files at the given path. If not present the '$HOME/.workwatch' value is used."
    }
  ],
  "caption": "WorkWatch configuration options"
}
```

Since data files are in JSON format, there is no need to add extentions to parameters setting filenames. The config file is also a JSON format but it requires to give the full name with extension because there are plans to add more config file formats.

All filename and data directory parameters can't be null. The program will display an error if you provide any incorrect configuration option. If you provide a `--config-file` option it has to contain a correct path to config file. Otherwise it will display that there is no config file at the given path.
