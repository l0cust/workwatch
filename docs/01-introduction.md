# 1. Intoduction

WorkWatch is a software for helping an honest worktime measurement and billing. It is not a usual time-tracker. In such tools you just enter hours' ranges and describe what you have done at that particular time. WorkWatch measures your worktime like a stopwatch but measures hours and minutes. Additionally, it will help you manage measured worktime and make decisions upon it during the workflow.

It is fully command-line program working on Linux terminal. Most of its commands are interactive. In interactive mode it uses a very basic full-screen interface. It doesn't draws any boxes, windows, buttons or other controls because they are found disturbing sometimes. In fact, WorkWatch uses only tables during worklog display and simple screens with information and scrolling functionality.

The program is beeing developped in JavaScript and Node.js. Care is taken to make it working on latest LTS versons of the platform. It is beeing written without any external libraries. It uses pure JavaScript and Node.js APIs.

WorkWatch uses codenames taken from the list of the highest mountain peaks in different countries, beginning from 1-meter high peaks on islands and finishing on Himalayas' ones. They are designated for major versions of the program but some minor versions could be also marked with these codenames depending on the development stage and program's stability.

WorkWatch is a free software. It is available under the GNU GPL license. It means that you can distribute and modify it as you like but you can't use it with any proprietary software.

If you like this project, use it and recommend it to your friends. It's the best way to prevent it from stealing. :D
