# WorkWatch

This is a user manual for WorkWatch version 0.0.1 (Mountain), a linux terminal program for honest worktime tracking and billing. It is part of the WorkWatch software so that it is beiing distributed within its package, repository or any receivable unit. The software and its documentation are also distributed under the same license.

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

WorkWatch is beeing developped and maintained by Artur (locust) Rutkwoski <locust@mailbox.org>

Copyright (C) 2020-2025 by Artur Rutkowski

## Table of Contents

1. [Introduction](./01-introducton.md)
2. [Invoking WorkWatch](./02-invoking.md)
3. [Time Measurement](./03-time_measurement.md)
4. [Configuration](./04-configuration.md)
5. [Files](./05-files.md)
6. [Diagnostics](./06-diagnostics.md)
7. [Known Bugs](./07-known_bugs.md)
