# 2. Invoking WorkWatch

WorkWatch is a fully command-line program. It reads options and commands as arguments given during running the program. It accepts some configuration options which are overriding the config file, environment variables and/or predefined values.

Options are easly noticeable due to the double dash sign ("--") before each options. Multiple words in the given option are separated by the signle dash sign ("-"). Example options may be: "--data-dir", "--config-file" and so on. Value-taking options are separated form their values by bank space or equal ("=") sign.

You can use shorter aliases for each option. Aliases are the single letter prepended with a single dash. Aliases can be grouped together only for boolean options. Boolean options don't take values, their presence in the command-line sets their values to true.

Options are naturally understood as arguments for the specific command or the WorkWatch itself. Command is the one-work action which is taken by the program. Commands are easly noticeable by seeing the words that aren't opitons and values.

Currently, WorkWatch has three ways of invoking.

- `workwatch [options] <command>`
- `workwatch --help`
- `workwatch --version`

The second and third ones are very easy. They display the help summary and version information respectively. If they are given among other options the rest of given options or commands are ignored. The order of supplying arguments is irrelevant Even if they are written together. If so, the "--help" takes precedence.

> Warning: These two boolean options will be replaced with corresponding "help" and "version" commands in future. You may considere them as deprecated.

The first way of invoking program is generic for taking actions. Giving any command makes WorkWatch to analyse and validate the given arguments. If you don't specify any command the given options are ignored unless they consist "--help" or "--version" among them. The program behaves in that way because all options are for configuration and they have no further usage if there is no command to execute.

Such behavior also helps to decide which data file to load, if any. The "start" and "reset" commands need both data files - the measurement and worklog - whereas the log commands needs only the last one. Additionally, the reseting also skips measurement file validation to enable any action necessary.

If you invoke the program without any arguments it will display the usage info which is identical to one described above.
