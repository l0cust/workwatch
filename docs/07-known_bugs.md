# 7. Known Bugs

WorkWatch is at very early and experimental state, so bugs are more than obvious. The childhood age of a project results in many fixes and corrections. This chapter will describe the most important bugs and problems, together with some migration suggestions.

The bugs listed here are those noticed by myself. Beside the development I am a user of this program so it is easier to see what goes wrong. If you want to inform me about any bug you discovered, you can do that by creating an [issue](https://gitlab.com/l0cust/workwatch/-/issues).

Currently, the WorkWatch has one serious bug. It will be fixed in version 0.0.2. The problem to fix is the wrong measurement handle due to long timer intervals.

The WorkWatch measures time using internal timer mechanisms built in the Node.js. They are the part of the event loop which is main thread and core of the asynchronous work of the platform. The timer asserts that a given code executes not sooner than the given time interval. The longer time interval the greater additional delay of code execution.

Solution is to make the timer interval as short as it is possible. The one millisecond is the shortest possible and it doesn't defers. Additionally, there is work to do for changing time conversion and handling to milliseconds, not minutes.

This is only bug to fix for now. If you find any other, create an issue and describe the problem.
