# 6. Diagnostics

If there is no error the WorkWatch should return 0 which means a successfull finish. Otherwise, there are following status codes:

1. A command-line arguments or config options are incorrect.
2. The data files contain incorrect data, syntax and so on.
3. The problem with TTY used during interactive mode.

All standard signals: SIGINT, SIGQUIT, SIGHUP and SIGTERM are handled during measurement. These all are handled in the same way - the measurement is beeing stopped and saved to measurement file.
