/**
 * This file is part of the WorkWatch, a Linux terminal program for honest
 * worktime tracking and billing.
 *
 * Copyright (C) 2020-2025 by Artur Rutkowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * WorkWatch is beeing developped and maintained by Artur (locust) Rutkwoski
 * <locust@mailbox.org>
 */

/**
 * This is a file defining a start command. This command is responsible
 * for time measurement. It is invoked by "workwatch start" to begin or
 * continue the measurement. It also saves the current state of measurement
 * in the measurement file.
 */
const utils = require("../utils.js");
const measurementData = require("../utils/measurement-data.js");
const createMeasure = require("../measure.js");
const terminalHelpers = require("../terminal-helpers.js");

async function start(parameters, appData) {
  let previousMinutes = measurementData.measuredMinutes(appData.timeMeasurement);
  let number = measurementData.measurementNumber(appData);
  let hoursRange = measurementData.measurementHoursRange(appData.timeMeasurement);
  let times = [];
  let allMinutes = previousMinutes;
  const measure = createMeasure(
    (minutes) => { // Handle "tick" event.
      allMinutes = previousMinutes + minutes;
      terminalHelpers.status({minutes: allMinutes}, appData.terminal);
    },
    (minutes, stopTime) => { // Handle "stop" event.
      times.push(stopTime);
      terminalHelpers.stopScreen({stopTime, minutes}, appData.terminal);
      if (allMinutes > previousMinutes) {
        terminalHelpers.stopScreen(
          {}, appData.terminal, {saving: true, status: "pre"}
        );
        hoursRange.push({from: times[0], to: times[1]});
        appData.timeMeasurement.update({
          time: utils.minutesNumberToTime(allMinutes),
          number,
          hoursRange,
          project: ``
        })
        .then(updateSuccess => {
          terminalHelpers.stopScreen(
            {measurementFilename: `${appData.config.dataDir}/${appData.config.measurementFilename}`},
            appData.terminal,
            {saving: true, status: "complete"}
          );
        })
        .catch(updateError => {
          throw updateError;
        });
      } else {
        terminalHelpers.stopScreen({}, appData.terminal, {});
      }
    }
  );
  measure.on("error", (error) => {
    throw error;
  });
  measure.on("start", (startTime) => {
    times.push(startTime);
    terminalHelpers.startScreen(
      {startTime, number, hoursRange},
      appData.terminal
    );
    terminalHelpers.status({minutes: allMinutes}, appData.terminal);
  });
  return true;
}
module.exports = start;