/**
 * This file is part of the WorkWatch, a Linux terminal program for honest
 * worktime tracking and billing.
 *
 * Copyright (C) 2020-2025 by Artur Rutkowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * WorkWatch is beeing developped and maintained by Artur (locust) Rutkwoski
 * <locust@mailbox.org>
 */

/**
 * This file is a main entry of the program. It runs a CLI subsystem which
 * parses command-line arguments and runs a particular command.
 */
const process = require("node:process");
const {CLI} = require("./cli/cli.js");

const application = new CLI();
application.run()
.then(applicationSuccess => {
  // Success means no error and boolean true value from command.
  process.exitCode = 0;
})
.catch(applicationFail => {
  // All errors produced by WorkWatch are instances of Error object.
  if (applicationFail instanceof Error) {
    // Use full-screen displaying not only for errors.
    CLI.appData.terminal.clearScreen();
    CLI.appData.terminal.title = "WorkWatch Version 0.0.1 (Mountain) - ERROR!";
    CLI.appData.terminal.appArea = `Error: ${applicationFail.message}`;
    // Assign the exit status codes according to errors category.
    if (
      applicationFail.code.includes("_CLI")
      || applicationFail.code.includes("_CONFIG")
    ) {
      process.exitCode = 1;
    }
    if (applicationFail.code.includes("_FILE")) {
      process.exitCode = 2;
    }
    if (applicationFail.code.includes("_TTY")) {
      process.exitCode = 3;
    }
  }
});
