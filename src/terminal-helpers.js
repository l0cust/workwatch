/**
 * This file is part of the WorkWatch, a Linux terminal program for honest
 * worktime tracking and billing.
 *
 * Copyright (C) 2020-2025 by Artur Rutkowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * WorkWatch is beeing developped and maintained by Artur (locust) Rutkwoski
 * <locust@mailbox.org>
 */

/**
 * This is an interactive terminal handling helper for the start command.
 * This is a provisional version of the visual and controlling parts'
 * separation model which aims to remove terminal handling from the code
 * which should focus on the concrete task.
 */
const utils = require("./utils.js");

module.exports = {
  status(data, terminal, options = null) {
    let currentTime = utils.minutesNumberToTime(data.minutes);
    terminal.status = `Current time: ${currentTime} (Measuring)`;
  },
  stopScreen(data, terminal, options = null) {
    if (!options) {
      terminal.clearScreen();
      terminal.title = "WorkWatch Version 0.0.1 (Mountain) - Saving Measurement";
      terminal.interactive = false;
      terminal.appArea = `The worktime measurement has been stopped at ${data.stopTime}.`;
      const workedTime = utils.minutesNumberToTime(data.minutes).split(":");
      const worked = `${Number(workedTime[0])} hours and ${Number(workedTime[1])} minutes`;
      terminal.appArea = `You've worked ${worked}.`;
    } else if (options.saving && options.status === "pre") {
      terminal.appArea = "Saving...";
    } else if (options.saving && options.status === "complete") {
      terminal.appArea = `The measurement has been saved to ${data.measurementFilename}.json`;
    } else {
      terminal.appArea = "The current measurement isn't exceeding a minute so no save is performed.";
    }
  },
  startScreen(data, terminal, options = null) {
    terminal.interactive = true;
    terminal.title = "WorkWatch Version 0.0.1 (Mountain) - Time Measurement";
    terminal.appArea = `The measurement of your worktime has been started at ${data.startTime}.`;
    terminal.appArea = `This is measurement number ${data.number} for today.`;
    if (data.hoursRange.length > 0) {
      terminal.appArea = `Continuing previous measurement started at ${data.hoursRange[0].from}.`;
      terminal.appArea = `Your past working hours are:\n${data.hoursRange.map(range => `${range.from} - ${range.to}`).join(",\n")}`;
    }
    terminal.appArea = "Press ^C to stop.";
  }
};