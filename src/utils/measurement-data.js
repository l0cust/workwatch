/**
 * This file is part of the WorkWatch, a Linux terminal program for honest
 * worktime tracking and billing.
 *
 * Copyright (C) 2020-2025 by Artur Rutkowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * WorkWatch is beeing developped and maintained by Artur (locust) Rutkwoski
 * <locust@mailbox.org>
 */

/**
 * This is a specialized data object obtaining information vital for measurement
 * managing in "start" command. It defines two helper functions for measurement
 * number and hoursRange stored in the measurement file.
 */
const timeData = require("./time-data.js");
const utils = require("../utils.js");

module.exports = {
  // It gets the meausrement number considering log entries for the
  // particular day.
  measurementNumber(appData) {
    let number;
    try {
      number = appData.timeMeasurement.number;
    } catch (error) {
      // Handle the case where the measurement file is empty which is very
      // obvious when starting a new measurement.
      if (error.code === "ERR_FILE_MEASUREMENT_NO_NUMBER_PROPERTY_OR_WRONG_VALUE") {
        // Find entries for this day in a log.
        let logDay = appData.log.days.find(
          day => day.day === timeData.correctDate
        );
        // It is first measurement in this day if there are no log entries.
        // Otherwise, the number of measurements + 1.
        number = (logDay === undefined)? 1 : logDay.measurements.length + 1;
      } else {
        throw error;
      }
    }
    return number;
  },

  // Gets the hoursRange when empty measurement.
  measurementHoursRange(timeMeasurement) {
    let hoursRange;
    try {
      hoursRange = timeMeasurement.hoursRange;
    } catch (error) {
      if (error.code === "ERR_FILE_MEASUREMENT_NO_HOURSRANGE_PROPERTY_OR_INVALID") {
        hoursRange = [];
      } else {
        throw error;
      }
    }
    return hoursRange;
  },

  // Get measured time up to now. If it is a new measurement the measured
  // time is 0 minutes.
  measuredMinutes(timeMeasurement) {
    let minutes;
    try {
      minutes = utils.timeToMinutesNumber(timeMeasurement.time);
    } catch (error) {
      if (error.code === "ERR_FILE_MEASUREMENT_NO_TIME_PROPERTY_OR_INVALID") {
        minutes = 0;
      } else {
        throw error;
      }
    }
    return minutes;
  }
};