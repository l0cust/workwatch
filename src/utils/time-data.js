/**
 * This file is part of the WorkWatch, a Linux terminal program for honest
 * worktime tracking and billing.
 *
 * Copyright (C) 2020-2025 by Artur Rutkowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * WorkWatch is beeing developped and maintained by Artur (locust) Rutkwoski
 * <locust@mailbox.org>
 */

/**
 * This is a specialized data object which receives time and date data needed
 * for time measurement and data validation. It contains getters and functions
 * for every vital piece of time information needed by the program.
 */
const constants = Object.create(null);
Object.defineProperties(constants, {
  "TIME_SEPARATOR": {
    value: ":",
    writable: false,
    enumerable: false,
    configurable: false
  },
  "SHORT_TIME_LENGTH": {
    value: 5,
    writable: false,
    enumerable: false,
    configurable: false
  },
  "SHORT_TIME_PATTERN": {
    value: /^\d{2}:\d{2}/,
    writable: false,
    enumerable: false,
    configurable: false
  },
  "END_TIME": {
    value: "23:59",
    writable: false,
    enumerable: false,
    configurable: false
  },
  "MINUTE_IN_MS": {
    value: 60000,
    writable: false,
    enumerable: false,
    configurable: false
  },
  "TIMER_INTERVAL_MS": {
    value: 1,
    writable: false,
    enumerable: false,
    configurable: false
  },
  "ISOSTRING_PART_SEPARATOR": {
    value: "T",
    writable: false,
    enumerable: false,
    configurable: false
  },
  "TIMEZONE_SYMBOL": {
    value: "Z",
    writable: false,
    enumerable: false,
    configurable: false
  }
});
Object.freeze(constants);

const timeData = {
  // Returns current or timestamp-specified time in local system time.
  correctTime(timestampMS = this.timeMS) {
    const date = new Date(timestampMS);
    // Get hours amount for current timezone assertion.
    const hours = (date.getHours() < 10)? "0" + date.getHours() : date.getHours();
    return date.toISOString()
      .split(this.constants.ISOSTRING_PART_SEPARATOR)[1]
      .split(this.constants.TIME_SEPARATOR)
      .map(
        (stringPart, stringPartIndex) => (stringPartIndex === 0)? hours : stringPart
      )
      .join(this.constants.TIME_SEPARATOR)
      .replace(this.constants.TIMEZONE_SYMBOL, "");
  },
  
  // Gets the current time in hh:mm format used for the data files' content validation.
  correctTimeShort(timestampMS = this.timeMS) {
    return this.correctTime(timestampMS).substring(0, this.constants.SHORT_TIME_LENGTH);
  },

  // Gets current date in yyyy-mm-dd format.
  get correctDate() {
    return (new Date()).toISOString().split(this.constants.ISOSTRING_PART_SEPARATOR)[0];
  },
  
  // Gets the time when any time measurement has to stop.
  get absoluteEndTime() {
    return this.correctTime().replace(
      this.constants.SHORT_TIME_PATTERN,
      this.constants.END_TIME
    );
  },

  // Gets current time in milliseconds.
  get timeMS() {
    return Date.now();
  },

  // Gets the time in milliseconds when any time measurement has to stop.
  get absoluteEndTimeMS() {
    return Date.parse(
      this.correctDate + this.constants.ISOSTRING_PART_SEPARATOR + this.absoluteEndTime
    );
  },
  constants
};
Object.freeze(timeData);
module.exports = timeData;
